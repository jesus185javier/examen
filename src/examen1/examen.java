
package examen1;

/**
 *
 * @author jesus
 */
public class examen {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
      recibo recibo = new recibo();
        recibo.setNumRecibo("102");
        recibo.setNombre("Jose Lopez");
        recibo.setDomicilio("Av del Sol 1200");
        recibo.setFecha("21 Marzo 2019");
        recibo.setServicio(1);
        recibo.setKilowatts(2.00f);
        recibo.setKilowattsConsumidos(450);
        
        System.out.println("Ejemplo 1:");
        System.out.println("Num. Recibo: "+recibo.getNumRecibo());
        System.out.println("Nombre: "+recibo.getNombre());
        System.out.println("Domicilio: "+recibo.getDomicilio());
        System.out.println("Fecha: "+recibo.getFecha());
        System.out.println("Tipo Servicio: "+recibo.getServicio());
        System.out.println("Costo por kilowatts: "+recibo.getKilowatts());
        System.out.println("Kilowatts Consumidos: "+recibo.getKilowattsConsumidos());
        System.out.println("Sub Total: "+recibo.calcularSubTotal());
        System.out.println("Impuesto: "+recibo.calcularImpuesto());
        System.out.println("Total: "+recibo.calcularTotal());
        
        System.out.println("----------------------------------------------------------------------");
        
        recibo.setNumRecibo("103");
        recibo.setNombre("Abarrotes Feliz");
        recibo.setDomicilio("Av del Sol");
        recibo.setFecha("21 Marzo 2019");
        recibo.setServicio(2);
        recibo.setKilowatts(3.00f);
        recibo.setKilowattsConsumidos(1200);
        
        System.out.println("Ejemplo 2:");
        System.out.println("Num. Recibo: "+recibo.getNumRecibo());
        System.out.println("Nombre: "+recibo.getNombre());
        System.out.println("Domicilio: "+recibo.getDomicilio());
        System.out.println("Fecha: "+recibo.getFecha());
        System.out.println("Tipo Servicio: "+recibo.getServicio());
        System.out.println("Costo por kilowatts: "+recibo.getKilowatts());
        System.out.println("Kilowatts Consumidos: "+recibo.getKilowattsConsumidos());
        System.out.println("Sub Total: "+recibo.calcularSubTotal());
        System.out.println("Impuesto: "+recibo.calcularImpuesto());
        System.out.println("Total: "+recibo.calcularTotal());
        
        System.out.println("S E   F I N A L I Z O   E L   E X A M E N");
    }
    
}
  
 
    

